﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using MineSweeperProjeto;
using MineSweeperProjeto.Model;
using MineSweeperProjeto.Properties;
using MineSweeperProjeto.View;
using static MineSweeperProjeto.Program;
using Timer = System.Windows.Forms.Timer;

namespace MineSweeperProjeto.Controller
{
	public class GameController
	{
		public static Timer Temporizador { get; private set; }

		private int timerCounter = 0;

		private bool SoundOnOrOFF { get; set; } = true;

		public GameController()
		{
			V_MineSweeperGame.ButtonPressed += OnButtonClicked;

			V_MineSweeperGame.AskToRevealAllPieces += Reveal;
			V_MineSweeperGame.AskToResetBoard += V_MineSweeperGame_AskToResetBoard;
			V_StartForm.ChangeDifficultyInGame += V_GameMode_ChangeDifficulty;
			V_StartForm.TurnSoundEffectsInGame += V_StartForm_TurnSoundEffectsInGame;

			Temporizador = new Timer();
			//V_JOGO.ResetTileGrid += Reset;
			SetupTimer();
		}

		private void V_MineSweeperGame_AskToResetBoard()
		{
			M_Grelha.ResetModelBoard();
			Temporizador.Stop();
			Temporizador.Dispose();
			Temporizador = new Timer();
			SetupTimer();
			V_MineSweeperGame.AtualizaNumeroMinasDisponiveis(M_Grelha.NumMinasTotal);
			//SetupTimer();
		}

		private void V_StartForm_TurnSoundEffectsInGame()
		{
			// Faz um flip ao valor do boolean
			SoundOnOrOFF = !SoundOnOrOFF;
		}

		private void V_GameMode_ChangeDifficulty(Dificuldade dificuldade)
		{
			M_Grelha.AlteraDificuldade(dificuldade);
			V_MineSweeperGame.AtualizaNumeroMinasDisponiveis(M_Grelha.NumMinasTotal);
		}

		private void SetupTimer()
		{
			V_MineSweeperGame.AtualizaTempo("00:00");
			// Temporizador
			Temporizador.Interval = 1000; //1 sec
			Temporizador.Tick += TimerTick;
			timerCounter = 0;
		}

		//private void Reset(object sender, MouseEventArgs e)
		//{
		//	foreach (Button Botao in V_JOGO.GetButtons())
		//	{
		//		MudaBackground(Botao, Resources.tile_0);
		//	}
		//	M_tileGrid = new TileGrid(dificuldade);
		//	timer = new Timer();
		//	V_JOGO.AtualizaNumeroMinasDisponiveis(M_tileGrid.NumMinasTotal);
		//	// TODO: BUG: A VELOCIDADE DO TEMPO DUPLICA???
		//	SetupTimer();
		//}

		// Revela o mapa completo
		private void Reveal()
		{
			foreach (Button Botao in V_MineSweeperGame.GetButtons())
			{
				GetCoordinates(Botao, out int x, out int y);
				Tile currentTile = M_Grelha.GetTile(new System.Drawing.Point(x, y));

				if (currentTile.Vazio == false && currentTile.temMina == false)
				{
					// TODO: Arranjar maneira de por isto mais bonito
					SwitchBackground(Botao, currentTile);
				}
				else if (currentTile.temMina == true)
				{
					MudaBackground(Botao, Resources.bomb);
				}
				else if (currentTile.Vazio == true)
				{
					MudaBackground(Botao, Resources.tile_0);
				}
			}
		}

		/// <summary>
		///  Muda A imagem de fundo do botão para a Imagem Bitmap pretendida
		/// </summary>
		private static void MudaBackground(Button botaoAtual, Bitmap Imagem)
		{
			botaoAtual.BackgroundImage = Imagem;
			botaoAtual.BackgroundImageLayout = ImageLayout.Stretch;
		}

		private void OnButtonClicked(object sender, MouseEventArgs e)
		{
			// Botão esquerdo, abre espaço
			if (e.Button == MouseButtons.Left)
			{
				Temporizador.Start();
				//// Obter Botão premido e guardar
				var botaoAtual = (sender as Button);

				// Obtém as Coordenadas do Tile
				GetCoordinates(botaoAtual, out int x, out int y);

				Tile currentTile = M_Grelha.GetTile(new System.Drawing.Point(x, y));

				if (currentTile.temMina == true)
				{
					//	Jogo perdido
					Temporizador.Stop();

					MudaBackground(botaoAtual, Resources.bomb);
					Reveal();
					BombaFimJogo();

					// O jogo acaba
					V_MineSweeperGame.Close();
				}
				else if (currentTile.Vazio == false && currentTile.Aberto == false)
				{
					// TODO: Arranjar maneira de por isto mais bonito
					SwitchBackground(botaoAtual, currentTile);

					currentTile.Aberto = true;
					if (SoundOnOrOFF == true)
					{
						Thread soundThread = new Thread(Sound.PlayOpenTile)
						{
							IsBackground = true
						};
						soundThread.Start();
					}

					// Se a condição for verdadeira a condição acaba
					if (M_Grelha.TestarFim(currentTile) == true)
					{
						GanhouJogo();
					}
					//soundThread.Abort();
				}
				// O jogo tem que abrir todos os vazios adjacentes.
				// Primeira Questão: Como pedir os botões adjacentes ao view?
				else if (currentTile.Vazio == true && currentTile.Aberto == false)
				{
					Flood_Fill(currentTile, V_MineSweeperGame.GetButton(currentTile.Ponto));
				}
			}

			// Botão direito, coloca flag
			else if (e.Button == MouseButtons.Right)
			{
				Button botaoAtual = (sender as Button);
				GetCoordinates(botaoAtual, out int x, out int y);

				// Obtém as Coordenadas do Tile
				Tile currentTile = Program.M_Grelha.GetTile(new System.Drawing.Point(x, y));

				if (currentTile.Aberto != true)
				{
					if (SoundOnOrOFF == true)
					{
						Thread soundThread = new Thread(Sound.PlayPutFlag)
						{
							IsBackground = true
						};
						soundThread.Start();
					}

					if (currentTile.Flagged == true)
					{
						MudaBackground(botaoAtual, Resources.unopened);
						currentTile.Flagged = false;
						M_Grelha.NumFlags--;
						V_MineSweeperGame.AtualizaNumeroMinasDisponiveis(M_Grelha.NumMinasTotal - M_Grelha.NumFlags);
					}
					else if (currentTile.Flagged == false)
					{
						MudaBackground(botaoAtual, Resources.flag1);
						currentTile.Flagged = true;
						M_Grelha.NumFlags++;
						V_MineSweeperGame.AtualizaNumeroMinasDisponiveis(M_Grelha.NumMinasTotal - M_Grelha.NumFlags);
					}
					//soundThread.Abort();
				}
				else
				{
					return;
				}
			}
		}

		private void OnButtonClicked(Button botaoAtual, Tile currentTile, MouseButtons left)
		{
			SwitchBackground(botaoAtual, currentTile);
			currentTile.Aberto = true;
			// Se a condição for verdadeira a condição acaba
			if (M_Grelha.TestarFim(currentTile) == true)
			{
				GanhouJogo();
			}
		}

		private static void SwitchBackground(Button botaoAtual, Tile currentTile)
		{
			switch (currentTile.NumeroMinas)
			{
				case 1: MudaBackground(botaoAtual, Resources.tile_1); break;
				case 2: MudaBackground(botaoAtual, Resources.tile_2); break;
				case 3: MudaBackground(botaoAtual, Resources.tile_3); break;
				case 4: MudaBackground(botaoAtual, Resources.tile_4); break;
				case 5: MudaBackground(botaoAtual, Resources.tile_5); break;
				case 6: MudaBackground(botaoAtual, Resources.tile_6); break;
				case 7: MudaBackground(botaoAtual, Resources.tile_7); break;
				case 8: MudaBackground(botaoAtual, Resources.tile_8); break;
				default:
					throw new ArgumentException("O número de minas adjacentes não pode ser superior a 8 nem inferior a 1");
			}
		}

		private void Flood_Fill(Tile currentTile, Button botaoAtual)
		{
			if (currentTile != null || botaoAtual != null)
			{
				if (currentTile.Aberto == true)
				{
					return;
				}
				else if (currentTile.Vazio == false)
				{
					OnButtonClicked(botaoAtual, currentTile, MouseButtons.Left);

					return;
				}
				else if (currentTile.Vazio == true && currentTile.Aberto == false)
				{
					currentTile.Aberto = true;
					// Se a condição for verdadeira o jogo acaba
					if (M_Grelha.TestarFim(currentTile) == true)
					{
						GanhouJogo();
					}

					MudaBackground(botaoAtual, Resources.tile_0);

					// Norte
					Point ponto = new Point(currentTile.Ponto.X, currentTile.Ponto.Y + 1);
					if (M_Grelha.PointIsValid(ref ponto))
						Flood_Fill(M_Grelha.GetTile(ponto), V_MineSweeperGame.GetButton(ponto));
					// Sul
					ponto = new Point(currentTile.Ponto.X, currentTile.Ponto.Y - 1);
					if (M_Grelha.PointIsValid(ref ponto))
						Flood_Fill(M_Grelha.GetTile(ponto), V_MineSweeperGame.GetButton(ponto));
					// Este
					ponto = new Point(currentTile.Ponto.X + 1, currentTile.Ponto.Y);
					if (M_Grelha.PointIsValid(ref ponto))
						Flood_Fill(M_Grelha.GetTile(ponto), V_MineSweeperGame.GetButton(ponto));

					// Oeste
					ponto = new Point(currentTile.Ponto.X - 1, currentTile.Ponto.Y);
					if (M_Grelha.PointIsValid(ref ponto))
						Flood_Fill(M_Grelha.GetTile(ponto), V_MineSweeperGame.GetButton(ponto));

					// Nordeste
					ponto = new Point(currentTile.Ponto.X + 1, currentTile.Ponto.Y + 1);
					if (M_Grelha.PointIsValid(ref ponto))
						Flood_Fill(M_Grelha.GetTile(ponto), V_MineSweeperGame.GetButton(ponto));

					// Noroeste
					ponto = new Point(currentTile.Ponto.X - 1, currentTile.Ponto.Y + 1);
					if (M_Grelha.PointIsValid(ref ponto))
						Flood_Fill(M_Grelha.GetTile(ponto), V_MineSweeperGame.GetButton(ponto));

					// Sudoeste
					ponto = new Point(currentTile.Ponto.X - 1, currentTile.Ponto.Y - 1);
					if (M_Grelha.PointIsValid(ref ponto))
						Flood_Fill(M_Grelha.GetTile(ponto), V_MineSweeperGame.GetButton(ponto));

					// Sudeste
					ponto = new Point(currentTile.Ponto.X + 1, currentTile.Ponto.Y - 1);
					if (M_Grelha.PointIsValid(ref ponto))
						Flood_Fill(M_Grelha.GetTile(ponto), V_MineSweeperGame.GetButton(ponto));
				}
				else
				{
					return;
				}
			}
			else
			{
				return;
			}
		}

		private void GanhouJogo()
		{
			Temporizador.Stop();
			if (SoundOnOrOFF == true)
			{
				Thread soundThread = new Thread(Sound.PlayWinning);
				soundThread.IsBackground = true;
				soundThread.Start();
			}

			// Abre uma messabox que informa o utilizador que ganhou o jogo
			MessageBox.Show("Muito bem: " + GetTimeString(), "Ganhou!", MessageBoxButtons.OK, MessageBoxIcon.Information);
			//soundThread.Abort();

			FormVencedor _vencedor = new FormVencedor();
			if (_vencedor.ShowDialog() == DialogResult.OK)
				// O jogo acaba
				V_MineSweeperGame.Close();
		}

		/// <summary>
		/// Função apresenta uma messagem de erro em como clicou numa mina a perdeu
		/// </summary>
		private void BombaFimJogo()
		{
			if (SoundOnOrOFF == true)
			{
				Thread soundThread = new Thread(Sound.PlayGameOver)
				{
					IsBackground = true
				};
				soundThread.Start();
			}//// Criar uma thread que funcione em background para emitir um som, neste caso o som de uma mina a explodir

			// Abre uma messabox que informa o utilizador que perdeu o jogo
			MessageBox.Show("BOOOM!", "Perdeu o jogo", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
			//soundThread.Abort();
		}

		// O nome do botão é do género {x}-{y}, a partir desse facto vamos extrair os números para variáveis int
		private static void GetCoordinates(Button botaoAtual, out int x, out int y)
		{
			var parts = botaoAtual.Name.Split('-');
			x = Convert.ToInt32(parts[0]);
			y = Convert.ToInt32(parts[1]);
		}

		public string GetTimeString()
		{
			//create time span from our counter
			TimeSpan time = TimeSpan.FromSeconds(timerCounter);

			//format that into a string
			string timeString = time.ToString(@"mm\:ss");

			//return it
			return timeString;
		}

		private void TimerTick(object sender, EventArgs e)
		{
			timerCounter++;
			V_MineSweeperGame.AtualizaTempo(GetTimeString());
		}
	}
}