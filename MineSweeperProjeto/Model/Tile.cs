﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MineSweeperProjeto.Properties;

namespace MineSweeperProjeto
{
	public class Tile
	{
		// Nome do "tile"
		private string name = string.Empty;

		// Ponto de coordenadas do "tile"
		private Point ponto;

		public Point Ponto
		{
			get { return ponto; }
			set
			{
				this.ponto = value;
			}
		}

		// Variável pode adquirir os seguintes valores:
		// True: O quadrado foi assinalado pelo utilizador. (Com o rato direito - em principio)
		// False: O quadrado encontra-se no seu estado normal, o utilizador ainda não teve interação com este.

		private bool flagged;

		// Propriedade flagged
		internal bool Flagged
		{
			get => this.flagged;
			set
			{
				this.flagged = value;
			}
		}

		// Indica se estamos perante um tile vazio
		public bool Vazio { get; set; } = false;

		// Números de minas que se encontram à volta do "tile"
		private int numeroMinas;

		public int NumeroMinas
		{
			get { return numeroMinas; }
			set
			{
				if (value >= 0 && value <= 8)
					numeroMinas = value;
				else
				{
					throw new ArgumentOutOfRangeException("Número inválido de minas adjacentes");
				}
			}
		}

		// Indica se o "tile" possui Mina, True: Possui, False: Não Possui
		public bool temMina { get; set; } = false;

		// Indica se o "tile" já foi aberto"
		public bool Aberto { get; set; } = false;

		// Coordenadas adjacentes vão ser obtidas a partir de cada soma a um desses pontos a cada iteração
		public static readonly Point[] adjacentCoords =
		{
			new Point (-1, -1),
			new Point (0, -1),
			new Point (1, -1),
			new Point (1, 0),
			new Point (1, 1),
			new Point (0, 1),
			new Point (-1,1),
			new Point (-1, 0)
		};

		public Tile(Point _ponto)
		{
			// Verifica se o ponto recebido está vazio
			//if (_ponto != Point.)
			//{
			this.ponto = _ponto;
			this.name = string.Format($"{_ponto.X}-{_ponto.Y}");
			//}
			// Lança uma excepção caso o ponto estão vazio
			//else
			//{
			//	throw new ArgumentNullException("Ponto do tile encontra-se vazio");
			//}
		}

		public Tile(string _nome, Point _ponto)
		{
			// Verifica se o conteúdo recebido por paramatros são nulos ou vazios
			if (string.IsNullOrEmpty(_nome) /*&& _ponto != Point.Empty*/)
			{
				this.name = _nome;
				this.ponto = _ponto;
			}
			// Lança uma excepção caso o nome e o ponto estão vazios
			else
			{
				throw new ArgumentNullException("Nome e Ponto do tile encontram-se vazios");
			}
		}
	}
}